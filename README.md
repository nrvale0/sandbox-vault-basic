# sandbox-vault-basic

An environment for playing with a basic Vault setup. Includes scripts for basic (non-production) Vault + Consul setup and Vault init.

# Usage

First start the Vagrant VM:

```pre
$ vagrant up
<wait for Vagrant VM>
```

In the first terminal, start a Consul server in dev mode(single server cluster):

```pre
$ vagrant ssh
$ /vagrant/consul-dev.sh
+ consul agent -dev -client 0.0.0.0
==> Starting Consul agent...
==> Consul agent running!
           Version: 'v0.9.0'
           Node ID: '29e48b8c-e9a8-45e7-87f2-fa2464192907'
         Node name: 'ubuntu-xenial'
        Datacenter: 'dc1'
            Server: true (bootstrap: false)
       Client Addr: 0.0.0.0 (HTTP: 8500, HTTPS: -1, DNS: 8600)
      Cluster Addr: 127.0.0.1 (LAN: 8301, WAN: 8302)
    Gossip encrypt: false, RPC-TLS: false, TLS-Incoming: false

==> Log data will now stream in as it occurs:

    2017/07/27 18:56:57 [DEBUG] Using random ID "29e48b8c-e9a8-45e7-87f2-fa2464192907" as node ID
    2017/07/27 18:56:57 [INFO] raft: Initial configuration (index=1): [{Suffrage:Voter ID:127.0.0.1:8300 Address:127.0.0.1:8300}]
    2017/07/27 18:56:57 [INFO] raft: Node at 127.0.0.1:8300 [Follower] entering Follower state (Leader: "")
    2017/07/27 18:56:57 [INFO] serf: EventMemberJoin: ubuntu-xenial.dc1 127.0.0.1
    2017/07/27 18:56:57 [INFO] serf: EventMemberJoin: ubuntu-xenial 127.0.0.1
    2017/07/27 18:56:57 [INFO] consul: Adding LAN server ubuntu-xenial (Addr: tcp/127.0.0.1:8300) (DC: dc1)
    2017/07/27 18:56:57 [INFO] consul: Handled member-join event for server "ubuntu-xenial.dc1" in area "wan"
    2017/07/27 18:56:57 [INFO] agent: Started DNS server 0.0.0.0:8600 (udp)
    2017/07/27 18:56:57 [INFO] agent: Started DNS server 0.0.0.0:8600 (tcp)
    2017/07/27 18:56:57 [INFO] agent: Started HTTP server on [::]:8500
    2017/07/27 18:56:57 [ERR] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed, error: CheckID "vault:127.0.0.1:8200:vault-sealed-check" does not have associated TTL from=127.0.0.1:51312
    2017/07/27 18:56:57 [DEBUG] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed (154.901µs) from=127.0.0.1:51312
    2017/07/27 18:56:57 [WARN] raft: Heartbeat timeout from "" reached, starting election
    2017/07/27 18:56:57 [INFO] raft: Node at 127.0.0.1:8300 [Candidate] entering Candidate state in term 2
    2017/07/27 18:56:57 [DEBUG] raft: Votes needed: 1
    2017/07/27 18:56:57 [DEBUG] raft: Vote granted from 127.0.0.1:8300 in term 2. Tally: 1
    2017/07/27 18:56:57 [INFO] raft: Election won. Tally: 1
    2017/07/27 18:56:57 [INFO] raft: Node at 127.0.0.1:8300 [Leader] entering Leader state
    2017/07/27 18:56:57 [INFO] consul: cluster leadership acquired
    2017/07/27 18:56:57 [INFO] consul: New leader elected: ubuntu-xenial
    2017/07/27 18:56:57 [DEBUG] consul: reset tombstone GC to index 3
    2017/07/27 18:56:57 [INFO] consul: member 'ubuntu-xenial' joined, marking health alive
    2017/07/27 18:56:57 [INFO] agent: Synced node info
    2017/07/27 18:56:58 [ERR] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed, error: CheckID "vault:127.0.0.1:8200:vault-sealed-check" does not have associated TTL from=127.0.0.1:51312
    2017/07/27 18:56:58 [DEBUG] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed (167.617µs) from=127.0.0.1:51312
    2017/07/27 18:56:59 [ERR] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed, error: CheckID "vault:127.0.0.1:8200:vault-sealed-check" does not have associated TTL from=127.0.0.1:51312
    2017/07/27 18:56:59 [DEBUG] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed (305.428µs) from=127.0.0.1:51312
    2017/07/27 18:57:00 [ERR] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed, error: CheckID "vault:127.0.0.1:8200:vault-sealed-check" does not have associated TTL from=127.0.0.1:51312
    2017/07/27 18:57:00 [DEBUG] http: Request PUT /v1/agent/check/fail/vault:127.0.0.1:8200:vault-sealed-check?note=Vault+Sealed (625.106µs) from=127.0.0.1:51312
    2017/07/27 18:57:01 [DEBUG] http: Request GET /v1/catalog/service/vault?stale= (502.377µs) from=127.0.0.1:51312
    2017/07/27 18:57:01 [INFO] agent: Synced service 'vault:127.0.0.1:8200'
    2017/07/27 18:57:01 [DEBUG] agent: Node info in sync
    2017/07/27 18:57:01 [DEBUG] http: Request PUT /v1/agent/service/register (1.742987ms) from=127.0.0.1:51312
    2017/07/27 18:57:01 [DEBUG] agent: Service 'vault:127.0.0.1:8200' in sync
```

In another terminal, start the Vault server. Note that the script points Vault to the Consul server:

```pre
$ vagrant ssh
$ /vagrant/vault-basic.sh
++ dirname /vagrant/vault-basic.sh
+ vault server -log-level=debug -config=/vagrant/vault.hcl
==> Vault server configuration:

                     Cgo: disabled
         Cluster Address: https://127.0.0.1:8201
              Listener 1: tcp (addr: "0.0.0.0:8200", cluster address: "0.0.0.0:8201", tls: "disabled")
               Log Level: debug
                   Mlock: supported: true, enabled: true
        Redirect Address: http://127.0.0.1:8200
                 Storage: consul (HA available)
                 Version: Vault v0.7.3
             Version Sha: 0b20ae0b9b7a748d607082b1add3663a28e31b68

==> Vault server started! Log data will stream in below:

2017/07/27 19:00:05.029699 [DEBUG] physical/consul: config path set: path=vault
2017/07/27 19:00:05.029762 [WARN ] physical/consul: appending trailing forward slash to path
2017/07/27 19:00:05.029783 [DEBUG] physical/consul: config disable_registration set: disable_registration=false
2017/07/27 19:00:05.029792 [DEBUG] physical/consul: config service set: service=vault
2017/07/27 19:00:05.029805 [DEBUG] physical/consul: config service_tags set: service_tags=
2017/07/27 19:00:05.029833 [DEBUG] physical/consul: config address set: address=127.0.0.1:8500
```

You can now go to the Consul UI to see the status of the Consul and Vault services. Note that Vault is Yellow/Orange because though it is healthy the Vault has neither been initialized nor unsealed:

![Consul UI screenshot - not yet initialized](./images/consul-ui-vault-not-initialized.jpg)

Now we can initialize the Vault:

```pre
$ /vagrant/vault-init.sh
+ VAULT_ADDR=http://localhost:8200
+ vault init -key-shares=2 -key-threshold=2
Unseal Key 1: jmygIK8yuI+4Jwd/aqiWK61S0qkxNvfEJZ1P1HhiuuQd
Unseal Key 2: IlVUnS/Hw8MWNM/jw2cpiQOhlVyktQQ7sptx01ntCKaQ
Initial Root Token: 900f9933-b05b-c73e-2e81-6697994d1a0b

Vault initialized with 2 keys and a key threshold of 2. Please
securely distribute the above keys. When the vault is re-sealed,
restarted, or stopped, you must provide at least 2 of these keys
to unseal it again.

Vault does not store the master key. Without at least 2 keys,
your vault will remain permanently sealed.
```

For the instance of Vault spun in this example, the unseal keys are shown above. For your instance, the keys will, of course, be different.

Now let's unseal the vault:

```pre
$ VAULT_ADDR=http://localhost:8200 vault unseal                                                                                                                                                                    Key (will be hidden):
Sealed: true
Key Shares: 2
Key Threshold: 2
Unseal Progress: 1
Unseal Nonce: eff6179b-3bb0-ddd0-7260-d0ec6afc029e

$ VAULT_ADDR=http://localhost:8200 vault unseal
Key (will be hidden):
Sealed: false
Key Shares: 2
Key Threshold: 2
Unseal Progress: 0
Unseal Nonce:

$ VAULT_ADDR=http://localhost:8200 vault status
Sealed: false
Key Shares: 2
Key Threshold: 2
Unseal Progress: 0
Unseal Nonce:
Version: 0.7.3
Cluster Name: vault-cluster-7a78f52d
Cluster ID: d6f9e074-bca2-d8a7-c1ad-309d21e14b18

High-Availability Enabled: true
        Mode: active
        Leader: http://127.0.0.1:8200
```

We can see the Vault is unsealed in the above output or we can also look at the Consul UI:

![Consul UI screenshot - unsealed](./images/consul-ui-vault-unsealed.jpg)
