#!/bin/bash

set -eux

VAULT_ADDR=http://localhost:8200 vault init -key-shares=2 -key-threshold=2
