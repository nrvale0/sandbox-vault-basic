#!/bin/bash

set -eux

vault server -log-level=debug -config="$(dirname $0)/vault.hcl"
